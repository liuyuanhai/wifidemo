package com.biyesheji.wifidemo.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

//import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;


public class FtpUtil {
    /** 只需要ip地址，不需要前面的ftp:// */
    private static final String HOST = "47.243.40.8";
    private static final int PORT = 21;
    private static String USERNAME = "jialia_cn";
    private static String PASSWORD = "li15017749367";
    private String gongGaoFtpAddress="a.txt";
    private FTPClient client;


    public void getGongGaoFile(Context context, OnGongGaoListener listener){
        new Thread(new Runnable() {
            @Override
            public void run() {
                client = new FTPClient();
                try {
                    client.connect(HOST, PORT);
                    client.login(USERNAME, PASSWORD);
                    Log.e("---------->","ftp 登录成功");
                    String dir = FileUtil.createAndroidQFilePath(context,"gonggao");
                    String gongGaoFilePath = dir+"gongGao.txt";
                    File gongGaoFile = new File(gongGaoFilePath);
                    client.setCharset("utf-8");
                    client.download(gongGaoFtpAddress,gongGaoFile);
                    Log.e("---------->","ftp 下载成功");
                    if(gongGaoFile!=null){
                        String s = FileUtil.readTxtFile(gongGaoFilePath);
                        Log.e("---------->","ftp 公告内容："+s);

                        listener.getGongGaoSuccess(s);
                    }
                } catch (Exception e) {
                    listener.getGongGaoFailed();

                    Log.e("---------->","ftp 登录失败"+e.getMessage());
                    e.printStackTrace();
                    return;
                }
            }
        }).start();
    }
    public interface OnGongGaoListener{
        public void getGongGaoSuccess(String gongGaoContent);
        public void getGongGaoFailed();

    }
}
