package com.biyesheji.wifidemo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Build;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by haiye on 2018/4/14.
 */

public class ImageDealUtil {
    /**
    * 旋转图片
    * @param angle 旋转角度
    * @param bitmap
    * @return Bitmap
    */
    public static Bitmap rotateBitmap(int angle , Bitmap bitmap) {
        //旋转图片 动作
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        // 创建新的图片
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    /**
     * 将图片文件压缩到指定的图片宽度内
     * @param f 文件
     * @para size 文件宽度（单位像素
     */
    public static Bitmap decodeFile(File f, int size) {
        try {
            // 解码图像大小
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // 找到正确的刻度值，它应该是2的幂。
            final int REQUIRED_SIZE = size;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            Log.e("------>","with = "+width_tmp+";height = "+height_tmp);
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }
    public static Bitmap readBitMap(Context context, int resId, boolean isPng) {
        if (isPng) {
            return readBitMapPng(context, resId);
        } else {
            return readBitMapJpg(context, resId);
        }
    }
    /**
     * 读取本地资源的图片
     * @param context 上下文
     * @param resId   图片ID
     * @param compressNumber 是压缩倍数(2的指数倍)
     * @return 控件显示宽度
     * width 图片显示的宽
     * height 图片显示的高
     */
    public static Bitmap readBitMapJpgComPress(Context context, int resId,int compressNumber) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        /**
         * 如果该值设为true那么将不返回实际的bitmap对象，
         * 不给其分配内存空间但是可以得到一些解码边界信息即图片大小等信息
         * 。因此我们可以通过设置inJustDecodeBounds为true，
         * 获取到outHeight(图片原始高度)和 outWidth(图片的原始宽度)，
         * 然后计算一个inSampleSize(缩放值)，就可以取图片了，这里要注意的是，inSampleSize 可能等于0，
         * 必须做判断。也就是说先将Options的属性inJustDecodeBounds设为true，
         * 先获取图片的基本大小信息数据(信息没有保存在bitmap里面，而是保存在options里面)，
         * 通过options.outHeight和 options. outWidth获取的大小信息以及自己想要到得图片大小计算出来缩放比例inSampleSize，
         * 然后紧接着将inJustDecodeBounds设为false，就可以根据已经得到的缩放比例得到自己想要的图片缩放图了。
         */
        opt.inJustDecodeBounds = true;
        InputStream input = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(input, null, opt);
        opt.inSampleSize =compressNumber; //缩略图大小为原始图片大小的几分之一
//		Log.e("opt.inSampleSize=","opt.outWidth="+opt.outWidth+";opt.outHeight="+opt.outHeight);
        opt.inJustDecodeBounds = false;//
        opt.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
        opt.inInputShareable = true;//设置 是否能够共享一个指向数据源的引用，或者是进行一份拷贝
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    /**
     * 读取本地资源的图片
     * @param context 上下文
     * @param resId   图片ID
     * @return 控件显示宽度
     * width 图片显示的宽
     * height 图片显示的高
     */
    public static Bitmap readBitMapJpg(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        /**
         * 如果该值设为true那么将不返回实际的bitmap对象，
         * 不给其分配内存空间但是可以得到一些解码边界信息即图片大小等信息
         * 。因此我们可以通过设置inJustDecodeBounds为true，
         * 获取到outHeight(图片原始高度)和 outWidth(图片的原始宽度)，
         * 然后计算一个inSampleSize(缩放值)，就可以取图片了，这里要注意的是，inSampleSize 可能等于0，
         * 必须做判断。也就是说先将Options的属性inJustDecodeBounds设为true，
         * 先获取图片的基本大小信息数据(信息没有保存在bitmap里面，而是保存在options里面)，
         * 通过options.outHeight和 options. outWidth获取的大小信息以及自己想要到得图片大小计算出来缩放比例inSampleSize，
         * 然后紧接着将inJustDecodeBounds设为false，就可以根据已经得到的缩放比例得到自己想要的图片缩放图了。
         */
        opt.inJustDecodeBounds = true;
        InputStream input = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(input, null, opt);
		opt.inSampleSize =1; //缩略图大小为原始图片大小的几分之一
//		Log.e("opt.inSampleSize=","opt.outWidth="+opt.outWidth+";opt.outHeight="+opt.outHeight);
        opt.inJustDecodeBounds = false;//
        opt.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
        opt.inInputShareable = true;//设置 是否能够共享一个指向数据源的引用，或者是进行一份拷贝
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }
    public static Bitmap readBitMapJpg(Context context, int resId,int width) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        /**
         * 如果该值设为true那么将不返回实际的bitmap对象，
         * 不给其分配内存空间但是可以得到一些解码边界信息即图片大小等信息
         * 。因此我们可以通过设置inJustDecodeBounds为true，
         * 获取到outHeight(图片原始高度)和 outWidth(图片的原始宽度)，
         * 然后计算一个inSampleSize(缩放值)，就可以取图片了，这里要注意的是，inSampleSize 可能等于0，
         * 必须做判断。也就是说先将Options的属性inJustDecodeBounds设为true，
         * 先获取图片的基本大小信息数据(信息没有保存在bitmap里面，而是保存在options里面)，
         * 通过options.outHeight和 options. outWidth获取的大小信息以及自己想要到得图片大小计算出来缩放比例inSampleSize，
         * 然后紧接着将inJustDecodeBounds设为false，就可以根据已经得到的缩放比例得到自己想要的图片缩放图了。
         */
        opt.inJustDecodeBounds = true;
        InputStream input = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(input, null, opt);
        int scale = 1;
        int imageWidth = opt.outWidth;
        while(imageWidth>2*width){
            imageWidth= imageWidth/2;
            scale=scale*2;
        }
//		opt.inSampleSize =2; //缩略图大小为原始图片大小的几分之一
//		Log.e("opt.inSampleSize=","opt.outWidth="+opt.outWidth+";opt.outHeight="+opt.outHeight);
        opt.inJustDecodeBounds = false;//
        opt.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
        opt.inInputShareable = true;//设置 是否能够共享一个指向数据源的引用，或者是进行一份拷贝
        opt.inSampleSize = scale;
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }
    public static Bitmap readBitMapPng(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
        /**
         * 如果该值设为true那么将不返回实际的bitmap对象，
         * 不给其分配内存空间但是可以得到一些解码边界信息即图片大小等信息
         * 。因此我们可以通过设置inJustDecodeBounds为true，
         * 获取到outHeight(图片原始高度)和 outWidth(图片的原始宽度)，
         * 然后计算一个inSampleSize(缩放值)，就可以取图片了，这里要注意的是，inSampleSize 可能等于0，
         * 必须做判断。也就是说先将Options的属性inJustDecodeBounds设为true，
         * 先获取图片的基本大小信息数据(信息没有保存在bitmap里面，而是保存在options里面)，
         * 通过options.outHeight和 options. outWidth获取的大小信息以及自己想要到得图片大小计算出来缩放比例inSampleSize，
         * 然后紧接着将inJustDecodeBounds设为false，就可以根据已经得到的缩放比例得到自己想要的图片缩放图了。
         */
        opt.inJustDecodeBounds = true;
        InputStream input = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(input, null, opt);
//		opt.inSampleSize =2; //缩略图大小为原始图片大小的几分之一
//		Log.e("opt.inSampleSize=","opt.outWidth="+opt.outWidth+";opt.outHeight="+opt.outHeight);
        opt.inJustDecodeBounds = false;//
        opt.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
        opt.inInputShareable = true;//设置 是否能够共享一个指向数据源的引用，或者是进行一份拷贝
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    public static Bitmap readBitMapPng(Context context, int resId,int width) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
        /**
         * 如果该值设为true那么将不返回实际的bitmap对象，
         * 不给其分配内存空间但是可以得到一些解码边界信息即图片大小等信息
         * 。因此我们可以通过设置inJustDecodeBounds为true，
         * 获取到outHeight(图片原始高度)和 outWidth(图片的原始宽度)，
         * 然后计算一个inSampleSize(缩放值)，就可以取图片了，这里要注意的是，inSampleSize 可能等于0，
         * 必须做判断。也就是说先将Options的属性inJustDecodeBounds设为true，
         * 先获取图片的基本大小信息数据(信息没有保存在bitmap里面，而是保存在options里面)，
         * 通过options.outHeight和 options. outWidth获取的大小信息以及自己想要到得图片大小计算出来缩放比例inSampleSize，
         * 然后紧接着将inJustDecodeBounds设为false，就可以根据已经得到的缩放比例得到自己想要的图片缩放图了。
         */
        opt.inJustDecodeBounds = true;
        InputStream input = context.getResources().openRawResource(resId);
        BitmapFactory.decodeStream(input, null, opt);
        int scale = 1;
        int imageWidth = opt.outWidth;
        while(imageWidth>2*width){
            imageWidth= imageWidth/2;
            scale=scale*2;
        }
//		opt.inSampleSize =2; //缩略图大小为原始图片大小的几分之一
//		Log.e("opt.inSampleSize=","opt.outWidth="+opt.outWidth+";opt.outHeight="+opt.outHeight);
        opt.inJustDecodeBounds = false;//
        opt.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
        opt.inInputShareable = true;//设置 是否能够共享一个指向数据源的引用，或者是进行一份拷贝
        opt.inSampleSize = scale;
        // 获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }
    /**
     * 得到bitmap的大小
     */
    public static int getBitmapSize(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {    //API 19
            return bitmap.getAllocationByteCount();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {//API 12
            return bitmap.getByteCount();
        }
        // 在低版本中用一行的字节x高度
        return bitmap.getRowBytes() * bitmap.getHeight();                //earlier version
    }
    public static Bitmap convertViewToBitmap(View view) {
        view.destroyDrawingCache();
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.setDrawingCacheEnabled(true);
        return view.getDrawingCache(true);
    }

    /**
     * 转换 Bitmap 为 Drawable
     * @param bitmap
     * @return
     */
    public static Drawable convertBitmapToDrawable(Bitmap bitmap){
        Drawable drawable = new BitmapDrawable(bitmap);
        return drawable;
    }
    /**
     * 转换 Bitmap 为 Drawable
     * @param bitmap
     * @return
     */
    public static Drawable convertBitmapToDrawable2(Context context, Bitmap bitmap){
        BitmapDrawable bd= new BitmapDrawable(context.getResources(), bitmap);
        return bd;
    }
    /**
     * 读取照片旋转角度 (调用系统图片时，Android7.0以后，部分手机会对图片进行一定角度的旋转)
     * @param path 照片路径
     * @return 角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }
    /**
     * 质量压缩方法
     *
     * @param image
     * @return
     */
    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        Log.e("--------->","before Compress length = " +baos.toByteArray().length /1024+"k");
        int options = 100;
        while (baos.toByteArray().length /1024 >200) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            //第一个参数 ：图片格式 ，第二个参数： 图片质量，100为最高，0为最差  ，第三个参数：保存压缩后的数据的流
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        Log.e("--------->","after Compress length = " +baos.toByteArray().length /1024+"k");
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }
    /**
     * 质量压缩方法
     * @param image
     * @return
     */
    public static boolean compressImageAndSave(Bitmap image,String path) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        Log.e("--------->","before Compress length = " +baos.toByteArray().length /1024+"k");
        int options = 100;
        while (baos.toByteArray().length /1024 >200) {  //循环判断如果压缩后图片是否大于200kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            //第一个参数 ：图片格式 ，第二个参数： 图片质量，100为最高，0为最差  ，第三个参数：保存压缩后的数据的流
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        Log.e("--------->","options = "+options+" after Compress length = " +baos.toByteArray().length /1024+"k");
        File file = new File(path);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baos.toByteArray());
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}

