package com.biyesheji.wifidemo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.StatFs;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by haiye on 2018/4/14.
 */

public class FileUtil {
    //是否存在sdcard.
    public static boolean isSdcard() {
        try {
            return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * 获取SD卡路径
     *
     * @return
     */
    public static String getSDCardPath()
    {
        return Environment.getExternalStorageDirectory().getAbsolutePath()
                + File.separator;
    }
    /**
     * 获取SD卡的容量 单位byte
     *
     * @return
     */
    public static long getSDCardAllSize()
    {
        if (isSdcard())
        {
            StatFs stat = new StatFs(getSDCardPath());
            // 获取空闲的数据块的数量
            long availableBlocks = (long) stat.getAvailableBlocks() - 4;
            // 获取单个数据块的大小（byte）
            long freeBlocks = stat.getAvailableBlocks();
            return freeBlocks * availableBlocks;
        }
        return 0;
    }
    /**
     * 计算sdcard上的剩余空间 * @return
     */
    public static int freeSpaceOnSd() {
        int MB = 1024 * 1024;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                .getPath());
        double sdFreeMB = ((double) stat.getAvailableBlocks() * (double) stat
                .getBlockSize()) / MB;
        return (int) sdFreeMB;
    }
    /**
     * 获取文件夹大小
     *
     * @param file File实例
     * @return long 单位为M
     * @throws Exception
     */
    public static long getFolderSize(File file) throws Exception {
        long size = 0;
        File[] fileList = file.listFiles();
        for (int i = 0; i < fileList.length; i++) {
            if (fileList[i].isDirectory()) {
                size = size + getFolderSize(fileList[i]);
            } else {
                size = size + fileList[i].length();
            }
        }
        return size;
    }
    /**
     * 保存 字符串 到文件中
     * filename 文件名
     * filecontent 文件内容
     */
    public static void saveToSDCard(String filename, String filecontent) throws Exception {
        File file = new File(filename);
        FileOutputStream outStream = new FileOutputStream(file);
        outStream.write(filecontent.getBytes());
        outStream.close();
    }

    /**
     * 读取assets下的txt文件，返回utf-8 String
     * @param context
     * @param fileName 不包括后缀
     * @return
     */
    public static String readAssetsTextReturnStr(Context context, String fileName){
        try {
            //Return an AssetManager instance for your application's package
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            // Convert the buffer into a string.
            String text = new String(buffer, "utf-8");
            // Finally stick the string into the text view.
            return text;
        } catch (IOException e) {
            // Should never happen!
//            throw new RuntimeException(e);
            e.printStackTrace();
        }
        return "";
    }
    /**
     * 读取文件中的字符串
     * filename 文件名
     */
    public static String readString(String filename)

    {
        String str;
        File file = new File(filename);
        try {
            FileInputStream in = new FileInputStream(file);
            // size  为字串的长度 ，这里一次性读完
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            str = new String(buffer, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return str;
    }
    /*
     * 创建文件夹 - 缓存目录文件夹
     * @param
     */
    public static String createFilePathCache(Context context, String s) {
        String path;
        if (isSdcard()) {
            File file = new File(context.getExternalCacheDir()+File.separator + context.getPackageName()+File.separator + s + File.separator);
            //如果文件夹不存在则创建
            if (!file.exists() || !file.isDirectory()) {
                file.mkdirs();
            }
            path = context.getExternalCacheDir()+File.separator + context.getPackageName()+File.separator + s + File.separator;
        } else {
            File file = new File(context.getCacheDir()+File.separator + context.getPackageName()+File.separator + s + File.separator);
            //如果文件夹不存在则创建
            if (!file.exists() || !file.isDirectory()) {
                file.mkdirs();
            }
            path = context.getCacheDir()+File.separator +context.getPackageName()+File.separator + s + File.separator;
        }
        return path;
    }

    /*
     * 创建文件夹 - 手机内存或sdcard中
     * @param
     */
    public static String createFilePath(Context context, String s) {
        String path;
        if (isSdcard()) {
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + context.getPackageName()+File.separator + s + File.separator);
            //如果文件夹不存在则创建
            if (!file.exists()||!file.isDirectory()) {
                file.mkdirs();
            }
            path = Environment.getExternalStorageDirectory()+File.separator + context.getPackageName() +File.separator+ s + File.separator;
        } else {
            File file = new File(Environment.getDataDirectory()+File.separator + context.getPackageName()+File.separator + s + File.separator);
            //如果文件夹不存在则创建
            if (!file.exists()||!file.isDirectory()) {
                file.mkdirs();
            }
            path = Environment.getDataDirectory()+File.separator +context.getPackageName()+File.separator + s + File.separator;
        }
        return path;
    }
    /*
     * 创建文件夹 - 10.0沙盒
     * @param
     */
    public static String createAndroidQFilePath(Context context, String s) {
        String path;
        path =context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)+File.separator+ s + File.separator;
        File file = new File(path);
        if(!file.exists()||!file.isDirectory()){
            file.mkdirs();
        }
        return path;
    }
    //文件长度 转换为适当单位。
    public static String formatFileSize(long length) {
        String result = null;
        int sub_string;
        if (length >= 1073741824) {
            sub_string = String.valueOf((float) length / 1073741824).indexOf(".");
            result = ((float) length / 1073741824 + "000").substring(0,
                    sub_string + 3)
                    + "GB";
        } else if (length >= 1048576) {
            sub_string = String.valueOf((float) length / 1048576).indexOf(".");
            result = ((float) length / 1048576 + "000").substring(0,
                    sub_string + 3)
                    + "MB";
        } else if (length >= 1024) {
            sub_string = String.valueOf((float) length / 1024).indexOf(".");
            result = ((float) length / 1024 + "000").substring(0,
                    sub_string + 3)
                    + "KB";
        } else if (length < 1024)
            result = Long.toString(length) + "B";
        return result;
    }

    //根据路径删除文件
    private static void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                if (item.isDirectory()) {
                    deleteFilesByDirectory(item);
                } else {
                    item.delete();
                }
            }
        }
    }

    /**
     * @param f    文件
     * @param size 压缩到的宽度
     * @return
     */
    public static Bitmap decodeFile(File f, int size) {
        try {
            // 解码图像大小
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // 找到正确的刻度值，它应该是2的幂。
//            final int REQUIRED_SIZE = 70;
            final int REQUIRED_SIZE = size;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            o=null;
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            o2.inPurgeable = true; //设置可回收 系统内存不足时可以被回收
            final Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            return b;
        } catch (FileNotFoundException e) {
        }
        return null;
    }
    /**
     * 保存bitmap到文件
     * @param path 文件保存路径
     * @param bm   bitmap
     * 质量压缩百分比 如：90 是压缩率，表示压缩10%; 如果不压缩是100，表示压缩率为0
     */
    public static void saveBitmapToFile(String path, Bitmap bm) {
        Log.e("----------->","path = "+path);
        Log.e("Bitmap", "开始保存");
        File f = new File(path);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            Log.e("Bitmap", "已经保存");
            f = null;
        } catch (FileNotFoundException e) {
            Log.e("Bitmap", "保存失败"+e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Bitmap", "保存失败"+e.getMessage());
            e.printStackTrace();
        }
    }
    /**
     * 拷贝图片文件
     * @param source   源文件
     * @param saveFile 保存到的文件
     * @param size     图片压缩后的像素宽度
     */
    public static void copyImageFile(File source, String saveFile, int size) {
        Bitmap b = decodeFile(source,size);
        FileUtil.saveBitmapToFile(saveFile, b);
        b.recycle();
        b = null;
        source = null;
    }
    /**
     * 文件拷贝
     * @param fromFile  原文件
     * @param toFile 拷贝到文件
     * @param rewrite 如果文件存在时候是否覆盖
     */
    public static void copyfile(File fromFile, File toFile, Boolean rewrite) {
        if (!fromFile.exists()) {
            return;
        }
        if (!fromFile.isFile()) {
            return;
        }
        if (!fromFile.canRead()) {
            return;
        }
        if (!toFile.getParentFile().exists()) {
            toFile.getParentFile().mkdirs();
        }
        if (toFile.exists() && rewrite) {
            toFile.delete();
        }
        //当文件不存时，canWrite一直返回的都是false
        // if (!toFile.canWrite()) {
        // MessageDialog.openError(new Shell(),"错误信息","不能够写将要复制的目标文件" + toFile.getPath());
        // Toast.makeText(this,"不能够写将要复制的目标文件", Toast.LENGTH_SHORT);
        // return ;
        // }
        try {
            FileInputStream fosfrom = new FileInputStream(fromFile);
            FileOutputStream fosto = new FileOutputStream(toFile);
            byte bt[] = new byte[1024];
            int c;
            while ((c = fosfrom.read(bt)) > 0) {
                fosto.write(bt, 0, c); //将内容写到新文件当中
            }
            fosfrom.close();
            fosto.close();
        } catch (Exception ex) {
            Log.e("readfile", ex.getMessage());
        }
    }
    /**
     * 文件拷贝 androidQ
     * @param fromPath  原文件
     * @param toFile 拷贝到文件
     * @param rewrite 如果文件存在时候是否覆盖
     */
    public static void QCopyfile(Context context,String fromPath, File toFile, Boolean rewrite) {

        try {
            ParcelFileDescriptor parcelFileDescriptor = null;
            parcelFileDescriptor = context.getContentResolver().openFileDescriptor(Uri.parse(fromPath), "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            if (!toFile.getParentFile().exists()) {
                toFile.getParentFile().mkdirs();
            }
            if (toFile.exists() && rewrite) {
                toFile.delete();
            }
            try {
                FileInputStream fosfrom = new FileInputStream(fileDescriptor);
                FileOutputStream fosto = new FileOutputStream(toFile);
                byte bt[] = new byte[1024];
                int c;
                while ((c = fosfrom.read(bt)) > 0) {
                    fosto.write(bt, 0, c); //将内容写到新文件当中
                }
                fosfrom.close();
                fosto.close();
            } catch (Exception ex) {
                Log.e("readfile", ex.getMessage());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
//    public static void copyFileQToAlbum(Context context,String filePath,String fileName){
//        File src = new File(filePath);
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.DESCRIPTION, "This is an image");
//        values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
//        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//        values.put(MediaStore.Images.Media.TITLE, "Image.jpg");
//        values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/");
//        Uri external = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        ContentResolver resolver = context.getContentResolver();
//        Uri insertUri = resolver.insert(external, values);
//        BufferedInputStream inputStream = null;
//        OutputStream os = null;
//        try {
//            inputStream = new BufferedInputStream(new FileInputStream(src));
//            if (insertUri != null) {
//                os = resolver.openOutputStream(insertUri);
//            }
//            if (os != null) {
//                byte[] buffer = new byte[1024 * 4];
//                int len;
//                while ((len = inputStream.read(buffer)) != -1) {
//                    os.write(buffer, 0, len);
//                }
//                os.flush();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                inputStream.close();
//                os.flush();
//                os.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//    public static void copyPrivateToAlbum(Context context,String orgFilePath,String displayName){
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.DESCRIPTION, "This is an image");
//        values.put(MediaStore.Images.Media.DISPLAY_NAME, displayName);
//        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//        values.put(MediaStore.Images.Media.TITLE, "Image.jpg");
//        values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/");
//        Uri external = null;//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            external = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        }
//        ContentResolver resolver = context.getContentResolver();
//        Uri insertUri = resolver.insert(external, values);//使用ContentResolver创建需要操作的文件
//        //Log.i("Test--","insertUri: " + insertUri);
//        InputStream ist=null;
//        OutputStream ost = null;
//        try {
//            ist=new FileInputStream(new File(orgFilePath));
//            if (insertUri != null) {
//                ost = resolver.openOutputStream(insertUri);
//            }
//            if (ost != null) {
//                byte[] buffer = new byte[4096];
//                int byteCount = 0;
//                while ((byteCount = ist.read(buffer)) != -1) {  // 循环从输入流读取 buffer字节
//                    ost.write(buffer, 0, byteCount);        // 将读取的输入流写入到输出流
//                }
//            }
//            Log.e("---------->","拷贝成功");
//        } catch (IOException e) {
//        } finally {
//            try {
//                if (ist != null) {
//                    ist.close();
//                }
//                if (ost != null) {
//                    ost.close();
//                }
//            } catch (IOException e) {
//            }
//        }
//        Log.e("---------->","拷贝完成");
//    }
    public static String getSystemAlbumPath(){
        String path = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_DCIM+File.separator;
        File file = new File(path+"Camera"+File.separator);
        if(file.exists()&&file.isDirectory()){
            file = null;
            return path+"Camera"+File.separator;
        }else{
            if(!file.exists()){
                file.mkdirs();
            }
        }
        return path;
    }
    /**
     * 获取系统图片文件夹路径
     * @return
     */
    public static String getSystemPictureDir(){
        String fileDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()+File.separator;
        return fileDir;
    }

    public static String readTxtFile(String strFilePath)
    {
        String path = strFilePath;
        String content = ""; //文件内容字符串
        //打开文件
        File file = new File(path);
        //如果path是传递过来的参数，可以做一个非目录的判断
        if (file.isDirectory())
        {
            Log.d("TestFile", "The File doesn't not exist.");
        }
        else
        {
            try {
                InputStream instream = new FileInputStream(file);
                if (instream != null)
                {
                    InputStreamReader inputreader = new InputStreamReader(instream);
                    BufferedReader buffreader = new BufferedReader(inputreader);
                    String line;
                    //分行读取
                    while (( line = buffreader.readLine()) != null) {
                        content += line + "\n";
                    }
                    instream.close();
                }
            }
            catch (java.io.FileNotFoundException e)
            {
                Log.d("TestFile", "The File doesn't not exist.");
            }
            catch (IOException e)
            {
                Log.d("TestFile", e.getMessage());
            }
        }
        return content;
    }
}
