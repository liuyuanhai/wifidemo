package com.biyesheji.wifidemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biyesheji.wifidemo.fragments.HomeFragment;
import com.biyesheji.wifidemo.fragments.HomeFragmentHomeHome;
import com.biyesheji.wifidemo.fragments.JihuoFragment;
import com.biyesheji.wifidemo.fragments.PinglunFragment;
import com.biyesheji.wifidemo.utils.FtpUtil;
import com.biyesheji.wifidemo.utils.ImageDealUtil;

public class MainActivity extends AppCompatActivity {
    private LinearLayout ll_home,ll_jihuo,ll_pinglun;
    private ImageView iv_home,iv_jihuo,iv_pinglun;
    private TextView tv_home,tv_jihuo,tv_pinglun;
    private LinearLayout ll_gonggao;
    private TextView tv_gonggao;
    private FragmentManager fm;
    private FragmentTransaction transaction;

    private HomeFragment homeFragment;
    private JihuoFragment jihuoFragment;
    private PinglunFragment pinglunFragment;
    private int fragmentPositon = -1;
    private Handler handler;
    private FtpUtil ftpUtil;
    private String gongGao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    private void init(){
        getAllViews();
        initHandler();
        initFragment();
        setListeners();
        selectPosition(0);
        //隐藏高公告
//        hideGonggao();
//        getGongGao();
    }
    private void getAllViews(){
        ll_home =findViewById(R.id.ll_home);
        ll_jihuo =findViewById(R.id.ll_jihuo);
        ll_pinglun =findViewById(R.id.ll_pinglun);
        iv_home =findViewById(R.id.iv_home);
        iv_jihuo =findViewById(R.id.iv_jihuo);
        iv_pinglun =findViewById(R.id.iv_pinglun);
        tv_home =findViewById(R.id.tv_home);
        tv_jihuo =findViewById(R.id.tv_jihuo);
        tv_pinglun =findViewById(R.id.tv_pinglun);

        ll_gonggao =findViewById(R.id.ll_gonggao);
        tv_gonggao =findViewById(R.id.tv_gonggao);
    }

    private void setListeners(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()){
                    case R.id.ll_home:
                        selectPosition(0);
                        break;
                    case R.id.ll_jihuo:
                        selectPosition(1);
                        break;
                    case R.id.ll_pinglun:
                        selectPosition(2);
                        break;
                }
            }
        };
        ll_home.setOnClickListener(listener);
        ll_jihuo.setOnClickListener(listener);
        ll_pinglun.setOnClickListener(listener);
    }
    private void initHandler(){
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                super.handleMessage(msg);
                switch(msg.what){
                    case 1://刷新公告
                        String gongGao = (String)msg.obj;
                        showGonggao(gongGao);
                        break;
                }
            }
        };
    }
    private void initFragment() {
        fm = this.getSupportFragmentManager();
    }

    private void selectPosition(int position){
        if(fragmentPositon==position){
            return;
        }
        changeUIShow(position);
        selectFragment(position);
        fragmentPositon =position;
    }
    private void getGongGao(){
        ftpUtil = new FtpUtil();
        ftpUtil.getGongGaoFile(this, new FtpUtil.OnGongGaoListener() {
            @Override
            public void getGongGaoSuccess(String gongGaoContent) {
                Message msg = new Message();
                msg.what = 1;
                msg.obj = gongGaoContent;
                handler.sendMessage(msg);
            }

            @Override
            public void getGongGaoFailed() {

            }
        });
    }
    private void changeUIShow(int position){
        if(position!=0){
            iv_home.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_home_unselected,false));
            tv_home.setTextColor(getResources().getColor(R.color.color999999));
        }else{
            iv_home.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_home_selected,false));
            tv_home.setTextColor(getResources().getColor(R.color.color333333));
        }
        if(position!=1){
            iv_jihuo.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_jihuo_unselected,false));
            tv_jihuo.setTextColor(getResources().getColor(R.color.color999999));
        }else{
            iv_jihuo.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_jihuo_selected,false));
            tv_jihuo.setTextColor(getResources().getColor(R.color.color333333));
        }
        if(position!=2){
            iv_pinglun.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_pinglun_unselected,false));
            tv_pinglun.setTextColor(getResources().getColor(R.color.color999999));
        }else{
            iv_pinglun.setImageBitmap(ImageDealUtil.readBitMap(this,R.mipmap.b_icon_pinglun_selected,false));
            tv_pinglun.setTextColor(getResources().getColor(R.color.color333333));
        }
    }

    private void selectFragment(int position) {
        fragmentPositon = position;
        transaction = fm.beginTransaction();
        if ((fragmentPositon != 0) && homeFragment != null) {
            transaction.hide(homeFragment);
        }
        if (fragmentPositon != 1 && jihuoFragment != null) {
            transaction.hide(jihuoFragment);
        }
        if(fragmentPositon!=2 && pinglunFragment!=null){
            transaction.hide(pinglunFragment);
        }
        switch (fragmentPositon) {
            case 0:
                if (homeFragment == null) {
                    homeFragment = new HomeFragment();
                    transaction.add(R.id.fl_content, homeFragment);
                }
                transaction.show(homeFragment);
                break;
            case 1:
                if (jihuoFragment == null) {
                    jihuoFragment = new JihuoFragment();
                    transaction.add(R.id.fl_content, jihuoFragment);
                }
                transaction.show(jihuoFragment);
                break;
            case 2:
                if (pinglunFragment == null) {
                    pinglunFragment = new PinglunFragment();
                    transaction.add(R.id.fl_content, pinglunFragment);
                }
                transaction.show(pinglunFragment);
                break;

        }
        transaction.commit();
    }
    private void showGonggao(String gonggaoContent){
        ll_gonggao.setVisibility(View.VISIBLE);
        tv_gonggao.setText(gonggaoContent);
    }
    private void hideGonggao(){
        ll_gonggao.setVisibility(View.GONE);
    }
}