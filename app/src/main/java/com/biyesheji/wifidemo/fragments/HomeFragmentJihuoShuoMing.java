package com.biyesheji.wifidemo.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import com.biyesheji.wifidemo.R;

public class HomeFragmentJihuoShuoMing extends Fragment {
    private View contentView;
    private View v_return;
    private WebView webview;
    private String url = "http://www.jialia.cn/tishi.html";//加载网页地址
    private HomeFragment.OnSelectFragmentListener onSelectFragmentListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        contentView = LayoutInflater.from(this.getActivity()).inflate(R.layout.web, null);
        init();
        return contentView;
    }

    private void init() {
        getAllViews();
        setListener();
        setWebView();
        webview.loadUrl(url);
    }

    private void getAllViews() {
        v_return = contentView.findViewById(R.id.v_return);
        webview = contentView.findViewById(R.id.webView);
    }

    private void setListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.v_return:
                        onSelectFragmentListener.selectFragment(0);
                        break;
                }
            }
        };
        v_return.setOnClickListener(listener);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {//transcation.hide和transcation.show会执行此方法
        super.onHiddenChanged(hidden);
        if (!isHidden()) {
            //可以调用刷新数据 或进行控件缩放
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView(){
        WebSettings webSettings = webview.getSettings();
        //支持javascript
        webSettings.setJavaScriptEnabled(true);
        // 设置可以支持缩放
        webSettings.setSupportZoom(true);
        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(false);
        //扩大比例的缩放
        webSettings.setUseWideViewPort(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        //如果不设置WebViewClient，请求会跳转系统浏览器
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            }

            public void onPageFinished(WebView view, String url) {//处理网页加载完成

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
            {
                //返回false，意味着请求过程里，不管有多少次的跳转请求（即新的请求地址），均交给webView自己处理，这也是此方法的默认处理
                //返回true，说明你自己想根据url，做新的跳转，比如在判断url符合条件的情况下，我想让webView加载http://ask.csdn.net/questions/178242
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (request.getUrl().toString().contains("sina.cn")){
                        view.loadUrl("http://ask.csdn.net/questions/178242");
                        return true;
                    }
                    if (request.getUrl().toString().startsWith("tel:")){
                        Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(request.getUrl().toString()));
                        startActivity(in);
                        return true;
                    }
                    if (request.getUrl().toString()!=null && request.getUrl().toString().startsWith("baidumap://")) {
                        Uri uri = Uri.parse(request.getUrl().toString());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return true;
                    }
                }
                return false;
            }

        });

    }
    public void setOnSelectFragmentListener(HomeFragment.OnSelectFragmentListener onSelectFragmentListener) {
        this.onSelectFragmentListener = onSelectFragmentListener;
    }
}
