package com.biyesheji.wifidemo.fragments;

import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.biyesheji.wifidemo.R;
import com.biyesheji.wifidemo.WebActivity;
import com.biyesheji.wifidemo.adapter.MarketAdapter;
import com.biyesheji.wifidemo.multidelegate.WifiDelegate;
import com.biyesheji.wifidemo.utils.PermissionUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.WIFI_SERVICE;

public class HomeFragmentHomeHome extends Fragment {
    private View contentView;
    private TextView tv_gonggao,tv_why_need_jihuo;
    private RecyclerView recyclerView;
    private List<ScanResult> list = new ArrayList<>();
    private MarketAdapter adapter;
    private HomeFragment.OnSelectFragmentListener onSelectFragmentListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        contentView = LayoutInflater.from(this.getActivity()).inflate(R.layout.fragment_home_home, null);
        init();
        Log.e("----------", "HomeFragment onCreateView");

        if (PermissionUtil.isHavePermission(getActivity(), PermissionUtil.TYPE.LOCATION)) {
           list.addAll(getWifiList());
           adapter.notifyDataSetChanged();
        }else{
            PermissionUtil.startRequestPermission(getActivity(), PermissionUtil.TYPE.LOCATION);
        }
        return contentView;
    }

    private void init() {
        getAllViews();
        setRecyclerView();
        setListener();
    }

    private void getAllViews() {
        recyclerView = contentView.findViewById(R.id.recyclerView);
        tv_why_need_jihuo = contentView.findViewById(R.id.tv_why_need_jihuo);
    }
    private void setListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.tv_why_need_jihuo:
                       onSelectFragmentListener.selectFragment(1);
                        break;
                }
            }
        };
        tv_why_need_jihuo.setOnClickListener(listener);
    }
    private void setRecyclerView(){
        adapter = new MarketAdapter(getActivity(),list);
        adapter.addItemViewDelegate(new WifiDelegate(getActivity()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {//transcation.hide和transcation.show会执行此方法
        super.onHiddenChanged(hidden);
        if (!isHidden()) {
            //可以调用刷新数据 或进行控件缩放
        }
    }
    public List<ScanResult> getWifiList() {
        Log.e("----------", "getWifiList");

        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(WIFI_SERVICE);
        List<ScanResult> scanWifiList = wifiManager.getScanResults();
        List<ScanResult> wifiList = new ArrayList<>();
        if (scanWifiList != null && scanWifiList.size() > 0) {
            HashMap<String, Integer> signalStrength = new HashMap<String, Integer>();
            for (int i = 0; i < scanWifiList.size(); i++) {
                ScanResult scanResult = scanWifiList.get(i);
                Log.e("tag", "搜索的wifi-ssid:" + scanResult.SSID);
                //得到的值是一个0到-100的区间值，是一个int型数据，其中0到-50表示信号最好，-50到-70表示信号偏差，小于-70表示最差，有可能连接不上或者掉线。
                Log.e("tag", "搜索的level:" + scanResult.level);
                if (!scanResult.SSID.isEmpty()) {
                    String key = scanResult.SSID + " " + scanResult.capabilities;
                    if (!signalStrength.containsKey(key)) {
                        signalStrength.put(key, i);
                        wifiList.add(scanResult);
                    }
                }
            }
        }else {
            Log.e("tag", "没有搜索到wifi");
        }
        return wifiList;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        PermissionUtil.onRequestPermissionsResult(requestCode,permissions,grantResults, PermissionUtil.TYPE.LOCATION,new PermissionUtil.PermissionGrantResutListener(){

            @Override
            public void grantSuccess(PermissionUtil.TYPE type) {
                list.addAll(getWifiList());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void grantFailed(PermissionUtil.TYPE type) {
                Toast.makeText(getActivity(),"申请权限失败",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setOnSelectFragmentListener(HomeFragment.OnSelectFragmentListener onSelectFragmentListener) {
        this.onSelectFragmentListener = onSelectFragmentListener;
    }
}
