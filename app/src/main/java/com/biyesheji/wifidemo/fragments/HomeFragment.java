package com.biyesheji.wifidemo.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.biyesheji.wifidemo.R;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeFragment extends Fragment {

    private FrameLayout fl_content;
    private FragmentManager fm;
    private FragmentTransaction transaction;

    private HomeFragmentHomeHome home;
    private HomeFragmentJihuoShuoMing jihuoShuoMing;
    private  OnSelectFragmentListener onSelectFragmentListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("--------------->", "onCreateView");
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.fragment_home, null);
        init(v);
        return v;
    }


    private void init(View v) {
        initFragment();
        setListener();
        selectFragment(0);
    }
    private void initFragment() {
        fm = this.getChildFragmentManager();
    }
    private void setListener(){
        onSelectFragmentListener = new OnSelectFragmentListener() {
            @Override
            public void selectFragment(int position) {
                HomeFragment.this.selectFragment(position);
            }
        };
    }
    private void selectFragment(int position) {
        transaction = fm.beginTransaction();
        if(position!=0&&home!=null){
            transaction.hide(home);
        }
        if(position!=1&&jihuoShuoMing!=null){
            transaction.hide(jihuoShuoMing);
        }
        switch (position){
            case 0:
                if (home == null) {
                    home = new HomeFragmentHomeHome();
                    home.setOnSelectFragmentListener(onSelectFragmentListener);
                    transaction.add(R.id.fl_content, home);
                }
                transaction.show(home);
                break;
            case 1:
                if (jihuoShuoMing == null) {
                    jihuoShuoMing = new HomeFragmentJihuoShuoMing();
                    jihuoShuoMing.setOnSelectFragmentListener(onSelectFragmentListener);
                    transaction.add(R.id.fl_content, jihuoShuoMing);
                }
                transaction.show(jihuoShuoMing);
                break;
        }
        transaction.commit();
    }
    public interface OnSelectFragmentListener{
        public void selectFragment(int position);
    }
}
