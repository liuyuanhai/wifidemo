package com.biyesheji.wifidemo.multidelegate;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biyesheji.wifidemo.R;
import com.biyesheji.wifidemo.utils.ImageDealUtil;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

public class WifiDelegate implements ItemViewDelegate {

    private Context con;
    public WifiDelegate(Context con) {
        this.con = con;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_wifi;
    }

    @Override
    public boolean isForViewType(Object item, int position) {
        return item instanceof ScanResult;
    }

    @Override
    public void convert(final ViewHolder holder, final Object o, int position) {
        ScanResult item = (ScanResult) o;

        ImageView iv_wifi =holder.getView(R.id.iv_wifi);
        TextView tv_wifi_name =holder.getView(R.id.tv_wifi_name);
        TextView tv_copy_password =holder.getView(R.id.tv_copy_password);
        LinearLayout ll_connect =holder.getView(R.id.ll_connect);
        tv_wifi_name.setText(item.SSID);
        if(item.level>-50){
            iv_wifi.setImageBitmap(ImageDealUtil.readBitMap(con,R.mipmap.b_icon_wifi_4,false));
        }else if(item.level<=-50&&item.level>-70){
            iv_wifi.setImageBitmap(ImageDealUtil.readBitMap(con,R.mipmap.b_icon_wifi_3,false));
        }else if(item.level<=-70&&item.level>-80){
            iv_wifi.setImageBitmap(ImageDealUtil.readBitMap(con,R.mipmap.b_icon_wifi_2,false));
        }else{
            iv_wifi.setImageBitmap(ImageDealUtil.readBitMap(con,R.mipmap.b_icon_wifi_1,false));
        }
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.tv_copy_password:
                        Toast.makeText(con,"请激活后再进行操作",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.ll_connect:
                        Toast.makeText(con,"请激活后再进行操作",Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        };
        tv_copy_password.setOnClickListener(listener);
        ll_connect.setOnClickListener(listener);
    }
}
