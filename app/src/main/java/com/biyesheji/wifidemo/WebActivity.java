package com.biyesheji.wifidemo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by Administrator on 2017/7/28 0028.
 */

public class WebActivity extends Activity {

    private WebView webView;
    private View v_return;
    boolean isErro;//加载是否出错
    private String title;
    private String url;//加载网页地址
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);
        title = this.getIntent().getStringExtra("title");
        url = this.getIntent().getStringExtra("url");
        Log.e("------------>","title = "+title+";url = "+url);
        init();
    }
    private void init(){
        getAllViews();
        setListener();
        setParam();
        setWebView();
//        ErroShowUtil.showLoading(this,re_erro);
        webView.loadUrl(url);
    }
    private void setParam(){
    }
    private void getAllViews(){
//        tv_title = findViewById(R.id.tv_title);
//        v_back = findViewById(R.id.v_back);
        v_return = findViewById(R.id.v_return);
        webView = findViewById(R.id.webView);
    }
    private void setListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()){
                    case R.id.v_return:
                        finish();
                        break;
                }
            }
        };
        v_return.setOnClickListener(listener);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView(){
        WebSettings webSettings = webView.getSettings();
        //支持javascript
        webSettings.setJavaScriptEnabled(true);
        // 设置可以支持缩放
        webSettings.setSupportZoom(true);
        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(false);
        //扩大比例的缩放
        webSettings.setUseWideViewPort(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        webView.addJavascriptInterface(new JavaScript(this), "android");
        //如果不设置WebViewClient，请求会跳转系统浏览器
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //该方法在Build.VERSION_CODES.LOLLIPOP以前有效，从Build.VERSION_CODES.LOLLIPOP起，建议使用shouldOverrideUrlLoading(WebView, WebResourceRequest)} instead
                //返回false，意味着请求过程里，不管有多少次的跳转请求（即新的请求地址），均交给webView自己处理，这也是此方法的默认处理
                //返回true，说明你自己想根据url，做新的跳转，比如在判断url符合条件的情况下，我想让webView加载http://ask.csdn.net/questions/178242
                if (url.startsWith("tel:")){
                    Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(in);
                    return true;
                }
                if (url!=null && url.startsWith("baidumap://")) {
                    try{
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }catch (Exception e){
                        Toast.makeText(WebActivity.this,"使用导航，需先安装百度地图",Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("------------>","onReceivedError");
                isErro = true;
            }

            public void onPageFinished(WebView view, String url) {//处理网页加载完成
                Log.e("------------>","onPageFinished");
                if(!isErro){
//                    ErroShowUtil.hieErroView(re_erro);
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
            {
                //返回false，意味着请求过程里，不管有多少次的跳转请求（即新的请求地址），均交给webView自己处理，这也是此方法的默认处理
                //返回true，说明你自己想根据url，做新的跳转，比如在判断url符合条件的情况下，我想让webView加载http://ask.csdn.net/questions/178242
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (request.getUrl().toString().contains("sina.cn")){
                        view.loadUrl("http://ask.csdn.net/questions/178242");
                        return true;
                    }
                    if (request.getUrl().toString().startsWith("tel:")){
                        Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(request.getUrl().toString()));
                        startActivity(in);
                        return true;
                    }
                    if (request.getUrl().toString()!=null && request.getUrl().toString().startsWith("baidumap://")) {
                        Uri uri = Uri.parse(request.getUrl().toString());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return true;
                    }
                }
                return false;
            }

        });

    }
    private class JavaScript {
        private Context context;
        public JavaScript(Context context){
            this.context = context;
        }


        /**
         * 配置套板单元时，打开素材选择页面选择素材的方法
         * @param materialType  选择素材的类型 ： 1 图片素材 ，2 视频素材
         * @param moudlePosition moudlePosition 套板单元的位置
         * @param anchorPoisition 锚的位置
         */
        @JavascriptInterface
        public void openMaterialsPageToSelect(int materialType,int moudlePosition,int anchorPoisition){
            Toast.makeText(WebActivity.this,"打开素材选择页面",Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void payWeixin(String data){
            Log.e("------------>","微信支付调用");
            Log.e("------------>",data);

        }
        @JavascriptInterface
        public String forward_to_page(String data) {//js调用时的写法 window.android.forward_to_page(data)
            Log.e("----------->","data = "+data);
            try {
//                Gson gson = new Gson();
//                JSData result = gson.fromJson(data, JSData.class);
//                if (result.getForward_type() == 11) {
//                    Intent web = new Intent();
//                    web.putExtra("title",getResources().getString(R.string.guid));
//                    web.putExtra("url", BuildConfig.BASE_URL+ RxUrl.guid);
//                    SystemUtil.gotoActivity(WebActivity.this,WebForGuidActivity.class,web);
//                }
            } catch (Exception e) {

            }
            return "{}";
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Runtime.getRuntime().gc();
        webView.clearCache(true);
//        System.exit(0);
    }
}
